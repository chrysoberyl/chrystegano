#!/usr/bin/env python3

import argparse
import os
import sys

from PIL import Image

GOOD_MODES = ["RGB", "RGBA", "L"] # TODO: support 1, (P)alette, CMYK, YCbCr, LAB, HSV, I, F

def analyse_lsb(image_object, image_file):
    print("Error: LSB bit data extraction not implemented yet")
    sys.exit(1)

def analyse_bits(image_object, image_file):
    analysis_dir = "chrystegano_out"
    try:
        os.makedirs(analysis_dir)
    except Exception as e:
        pass

    image_mode = image_object.mode

    if image_mode not in GOOD_MODES:
        print("I don't know how to analyse bytes in {} mode...".format(image_mode))
        sys.exit(1)

    if image_mode in ["RGB", "RGBA"]:
        for colour in ["R", "G", "B"]:
            for bit in range(0, 8):
                analyse_bit(image_object, '/'.join([analysis_dir, image_file]), colour, bit)
    elif image_mode in ["L"]:
        out_filename = os.path.join(analysis_dir, "bw_bytes.txt")
        with open(out_filename, "w") as ofh:
            pixels_in = image_object.load()
            xsize, ysize = image_object.size
            for x in range(xsize):
                for y in range(ysize):
                    print(pixels_in[x, y])

def analyse_bit(image_object, image_filename, colour, bit):
    file_split = image_filename.split('.')
    file_prefix = '.'.join(file_split[:-1])
    out_file = "{}_{}{:02d}.{}".format(file_prefix, colour, bit, file_split[-1])
    print("Processing: " + out_file)

    image_mode = image_object.mode
    if image_mode not in GOOD_MODES:
        print("I don't understand image mode \"{}\"... sorry!".format(image_mode))
        return

    xsize, ysize = image_object.size
    pixels_in = image_object.load()

    if image_mode in ["RGB", "RGBA"]:
        img_out = Image.new(image_mode, image_object.size)
        pixels_out = img_out.load()

        for x in range(xsize):
            for y in range(ysize):
                if image_mode == "RGB":
                    r, g, b = pixels_in[x, y]
                elif image_mode == "RGBA":
                    r, g, b, a = pixels_in[x, y]
                if colour == "R":
                    if (r >> bit) & 0x01:
                        pixels_out[x, y] = (255, 255, 255)
                    else:
                        pixels_out[x, y] = (0, 0, 0)
                elif colour == "G":
                    if (g >> bit) & 0x01:
                        pixels_out[x, y] = (255, 255, 255)
                    else:
                        pixels_out[x, y] = (0, 0, 0)
                elif colour == "B":
                    if (b >> bit) & 0x01:
                        pixels_out[x, y] = (255, 255, 255)
                    else:
                        pixels_out[x, y] = (0, 0, 0)
                else:
                    print("Bad colour given: {}".format(colour))
                    sys.exit(1)
        img_out.save(out_file)
    else:
        print("I don't know how to analyse bits in {} mode...".format(image_mode))

def analyse_image_file(image_file, analysis_mode):
    try:
        image = Image.open(image_file)
        image_filename = os.path.basename(image_file)
        if analysis_mode == 'lsb':
            analyse_lsb(image, image_filename)
        elif analysis_mode == 'fullscan':
            analyse_bits(image, image_filename)
    except FileNotFoundError as fnfe:
        print(fnfe)
        sys.exit(1)
    except OSError as ose:
        print(ose)
        sys.exit(1)

def analyse_audio_file(audio_file, analysis_mode):
    print("Error: audio analysis not supported yet!")
    sys.exit(1)

def _parse_args():
    parser = argparse.ArgumentParser()

    mode_group = parser.add_mutually_exclusive_group()
    mode_group.add_argument('--im', '--image-analysis-mode',
                            dest='image_analysis_mode',
                            help='Image analysis mode',
                            choices=['fullscan', 'lsb'])
    mode_group.add_argument('--am', '--audio-analysis-mode',
                            dest='audio_analysis_mode',
                            help='Audio analysis mode',
                            choices=['fullscan'])

    parser.add_argument('input_file',
                        help='The file to analyse')

    args = parser.parse_args()
    if not args.image_analysis_mode or args.audio_analysis_mode:
        print("Neither image nor audio analysis mode given:")
        print("defaulting to full scan of image.")
        args.image_analysis_mode = 'fullscan'
    return args

def main():
    args = _parse_args()
    if args.image_analysis_mode:
        analyse_image_file(args.input_file, args.image_analysis_mode)
    elif args.audio_analysis_mode:
        analyse_audio_file(args.input_file, args.audio_analysis_mode)

if __name__ == '__main__':
    main()
